<?php
/**
 * Plugin Name: WP Posting Status
 * Author: Nitish Takzaria
 * Author URI:https://nitish-web.jimdosite.com/
 * Description: Allow posting a status message on the WordPressadmin dashboard page
 * Version: 1.0
 * Text Domain : posting_status
 */

define( 'PS_URL', plugins_url( '', __FILE__ ) );
define( 'PS_DIR', trailingslashit( dirname( __FILE__ ) ) );

/*define plugin version*/
define( 'PS_VERSION', '1.0' );

// inclides inc folder file
require_once PS_DIR . 'inc/class-poststatus.php';




/**
 * Class object
 */
add_action( 'init', 'wp_posting_status' );

function wp_posting_status() {
	global $wp_status;
	$wp_status = new PostStatus();
}

// Register plugin activation hook and set plugin version
register_activation_hook( __FILE__, 'ps_activated' );

function ps_activated() {

	$version = get_option( 'ps_version' );
	if ( ! $version || PS_VERSION !== $version ) {
		update_option( 'ps_version', PS_VERSION );
	}
}
