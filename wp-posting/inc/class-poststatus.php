<?php

class PostStatus {
	
	/**
	 * Add hook and action 
	 */
	public function __construct() {
		
		// admin menu
		
		add_action( 'admin_menu', array( $this, 'register_my_custom_submenu_page' ) );

		add_action( 'admin_init', array( $this, 'general_admin_notice' ) );
		
	}

	/**
	 * Adds a Add status Sub page to Tool menu
	 */

	public function register_my_custom_submenu_page() {
    
		add_submenu_page(
            'tools.php', 'Submenu Page', 'Add status', 'manage_options', 'wp-posting-status', 
            array( $this, 'add_status_settings' )
        );
	}

 	/**
	 * Displays the add status field to admin and save status msg
	 */

	 public function add_status_settings(){

		$msg = '';
		
		if(isset($_POST['submit'])){
		if (  ! isset( $_POST['status_setting_submit_nonce'] )  || ! wp_verify_nonce( $_POST['status_setting_submit_nonce'], 'status_setting_submit' ) ) 
		{

		   wp_die( 'Invalid request' );
		
		} else {
		 
		   // process form data
			
			$status_msg = sanitize_text_field($_POST['admin_status_msg']);

			update_option('admin_status_msg',$status_msg);
			
			$msg  = esc_html__( 'Settings Updated', 'posting_status' );
		}	
	}


	?>
	<div class="form-wrap">
		<?php
		$page_title  = esc_html__( 'Add Status Message', 'posting_status' );
		if($msg != ''){
			echo '<h3>'.esc_html( $msg ).'</h3>';
		}
		?>
		<h3><?php echo esc_html( $page_title ); ?></h3>
		<form name="adminstatuspost" id="adminstatuspost" method="post" action="" class="validate">
			<table class="form-table">
				<tr>
					<th scope="row">
						<label for="sm_adminlabel"><?php echo esc_html__( 'Status Message', 'posting_status' ); ?></label>
					</th>
					<td>
					<?php $status_msg = get_option('admin_status_msg'); ?>
						<input name="admin_status_msg" id="admin_status_msg" type="text" value="<?php echo esc_attr( $status_msg ); ?>" size="30" required>
					</td>
				</tr>
			</table>
			<?php wp_nonce_field( 'status_setting_submit', 'status_setting_submit_nonce' ); ?>
			<p class="submit">
				<?php submit_button( 'Save', 'primary', 'submit', false ); ?>
			</p>
		</form>
	</div>
	<?php
	}
	
/**
 * Display Msg code for admin and network admin
 */

	public function general_admin_notice(){
	    global $pagenow;
	    if ( $pagenow == 'index.php' ) {
	    if(is_super_admin() || is_admin()) {
			add_action( 'admin_notices', array( $this, 'admin_status_notice' ) );
		 }
		}
	}

/**
 * Display Msg to admin on dashboard on main site and subsite
 */
	function admin_status_notice() {
		$status_msg = get_option('admin_status_msg');
		echo '<div class="notice notice-info is-dismissible"><p>' . esc_html__( $status_msg, 'posting_status' ) . '</p></div>';
	}

}
