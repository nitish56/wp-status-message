=== WP Posting Status ===
Contributors: Nitish Takzaria
Donate link: https://nitish-web.jimdosite.com/
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tested up to: 5.9

Give option to add admin to add status message 

== Description ==

This is a plugin which gives admin an option to add status which option would show under tools in the admin dashboard.
Once admin adds the status message admin can see the message on top of the dashboard.

== Installation ==

1. Upload the `user-tags` folder to the `/wp-content/plugins/` directory.
2. Activate the plugin through the 'Plugins' menu in WordPress.
3. To add new status meesage Go to Tools-> Add Status

== Supports Multisite ==

Note:
Only site admin and super admin can add status message

